
import React, { Component } from 'react';

class FiltroBusqueda extends Component {
    render() { 
        return (
            <ul className="custom-filtro-busqueda">
                <li className="custom-li-filtro">Fecha</li>
                <li className="custom-li-filtro">Lenguaje de programación</li>
            </ul>
        );
    }
}
 
export default FiltroBusqueda;
import React from 'react';
import { Link } from "react-router-dom";

const Politica = ( props ) => {
    return (
        <>
            <Link to="page/politica-privacidad">Política de privacidad</Link>
        </>
    )
}

export default Politica;